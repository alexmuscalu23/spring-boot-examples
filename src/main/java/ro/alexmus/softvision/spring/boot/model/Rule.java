package ro.alexmus.softvision.spring.boot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class Rule {

    @Getter
    @Setter
    @Id
    @GeneratedValue
    private Long id;
}
