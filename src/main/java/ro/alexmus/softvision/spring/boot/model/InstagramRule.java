package ro.alexmus.softvision.spring.boot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
public class InstagramRule extends Rule {

    @Getter
    @Setter
    private String hashTag;

    public InstagramRule() {

    }

    public InstagramRule(String hashTag) {
        this.hashTag = hashTag;
    }
}
