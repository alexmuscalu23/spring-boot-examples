package ro.alexmus.softvision.spring.boot.web;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ro.alexmus.softvision.spring.boot.model.Rule;

@Builder
public class RuleDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Class<? extends Rule> type;

    @Getter
    @Setter
    private String value;
}
