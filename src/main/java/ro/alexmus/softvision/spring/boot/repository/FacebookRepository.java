package ro.alexmus.softvision.spring.boot.repository;


import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ro.alexmus.softvision.spring.boot.model.FacebookRule;

@RepositoryRestResource(collectionResourceRel = "facebookRule", path = "facebookRule")
public interface FacebookRepository extends RuleRepository<FacebookRule>{
}
