package ro.alexmus.softvision.spring.boot.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.alexmus.softvision.spring.boot.model.InstagramRule;

@RestController
public class CustomController {

    @RequestMapping("/hello")
    public String hello(){
        return "Hello";
    }

    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable("name") String name){
        return "Hello " + name;
    }


    @RequestMapping("/json")
    public RuleDto simpleJson(){
        return RuleDto.builder().id(1L).type(InstagramRule.class).value("#asd").build();
    }
}
