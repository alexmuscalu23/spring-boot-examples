package ro.alexmus.softvision.spring.boot.repository;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ro.alexmus.softvision.spring.boot.model.InstagramRule;

@RepositoryRestResource(collectionResourceRel = "instagramRule", path = "instagramRule")
public interface InstagramRepository extends RuleRepository<InstagramRule> {
}
