package ro.alexmus.softvision.spring.boot.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ro.alexmus.softvision.spring.boot.model.Rule;

@RepositoryRestResource(collectionResourceRel = "rule", path = "rule")
public interface RuleRepository<T extends Rule> extends JpaRepository<T, Long> {
}
