package ro.alexmus.softvision.spring.boot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
public class FacebookRule extends Rule {

    @Getter
    @Setter
    private String pageId;

    public FacebookRule() {

    }

    public FacebookRule(String pageId){
        this.pageId = pageId;
    }
}
