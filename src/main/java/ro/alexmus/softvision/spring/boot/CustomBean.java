package ro.alexmus.softvision.spring.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomBean implements InitializingBean{

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Initializing MyBean");
    }

    public void doStuff(){
        log.info("This bean does nothing but it is loaded from Spring Context");
    }
}
